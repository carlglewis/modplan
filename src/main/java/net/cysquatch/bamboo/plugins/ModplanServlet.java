// ---------------------------------------
// Copyright (c) 2011, Dolby Laboratories.
// See LICENSE.TXT for full license terms. 
// ---------------------------------------

package net.cysquatch.bamboo.plugins;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ModplanServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        res.getWriter().write("What's this class for, anyway?");
        res.getWriter().close();
    }
}
