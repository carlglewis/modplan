
// ---------------------------------------
// Copyright (c) 2011, Dolby Laboratories.
// See LICENSE.TXT for full license terms. 
// ---------------------------------------

package net.cysquatch.bamboo.plugins;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "stringlist")
@XmlAccessorType(XmlAccessType.FIELD)
public class StringList {

    @XmlElementWrapper(name = "list")
    @XmlElement(name = "name")
    private List<String> strings = new ArrayList<String>();

    public StringList() {}


    public void add(String n) {
        strings.add(n);
    }
}

