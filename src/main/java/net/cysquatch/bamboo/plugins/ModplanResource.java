// ---------------------------------------
// Copyright (c) 2011, Dolby Laboratories.
// See LICENSE.TXT for full license terms. 
// ---------------------------------------

package net.cysquatch.bamboo.plugins;

import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.variable.VariableDefinitionFactory;
import com.atlassian.bamboo.variable.VariableDefinitionManager;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import java.lang.String;
import java.util.*;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.event.EventManager;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.chains.ChainStage;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.builder.Builder;
import com.atlassian.bamboo.repository.Repository;
import com.atlassian.bamboo.caching.DashboardCachingManager;
import com.atlassian.bamboo.utils.map.FilteredMap;
import com.atlassian.bamboo.task.TaskDefinition;
import org.apache.commons.collections.iterators.IteratorChain;
import org.apache.log4j.Logger;
import org.apache.commons.configuration.HierarchicalConfiguration;
import com.atlassian.bamboo.variable.VariableDefinitionManager;
import com.atlassian.bamboo.variable.VariableDefinition;
import com.atlassian.bamboo.variable.VariableDefinitionFactory;
import org.apache.tools.ant.types.Environment;

@Path("/{plankey}")
public class ModplanResource {
    
    private static final Logger log = Logger.getLogger(ModplanResource.class);
    private EventManager eventManager;
    private PlanManager planManager;
    private DashboardCachingManager dashboardCachingManager;
    private VariableDefinitionManager varMgr;
    private VariableDefinitionFactory varFactory;


    /**
     * This method will be called if no extra path information
     * is used in the request.
     * @param key optional key for the message
     * @return the message from the key parameter or the default message
     */
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getBuild(@PathParam("plankey") String plankey)
    {

        log.debug("ModplanResource.getBuild()");

        if (plankey == null) {
            log.info("plankey was null");
            return Response.ok(new Message("default","plankey was null")).build();
        }

        String mesg = "Return configuration for plan:  " + plankey;
        log.info(mesg);

        return Response.ok(new Message("default", mesg)).build();
    }


    // wget http://localhost:8085/rest/modplan/1.0/PROJ1-PLAN1/repository
    // Return a response containing all the configuration values for the
    // Plan-level repository 
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/repository")
    public Response getRepositoryConfig(@PathParam("plankey") String plankey)
    {
        log.info("ModplanResource.getRepositoryConfig()");

        try {
            Plan plan = getPlanByKey(plankey);
            return listRepositoryConfiguration(plankey);
        }
        catch(ResponseException re) {
            log.error("Exception thrown in ModplanResource.getRepositoryConfig()");
            return re.getResponse();
        }
    }


    // wget http://localhost:8085/rest/modplan/1.0/PROJ1-PLAN1/
    //     repository/repository.common.cleanCheckout
    // Get the value of a single repository configuration field
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/repository/{field}")
    public Response getRepositoryValue(
                    @PathParam("plankey") String plankey, 
                    @PathParam("field") String field)
    {
        log.info("ModplanResource.getRepositoryValue()");

        try {
            Plan plan = getPlanByKey(plankey);
            return getRepositoryConfigValue(plan, field);
        }
        catch(ResponseException re) {
            log.error("Exception thrown in ModplanResource.getRepositoryConfig()");
            return re.getResponse();
        }
    }


    //  curl -d <value> -H "Content-Type:text/plain"
    //      http://<host>:<port>/rest/modplan/1.0/PROJ1-PLAN1/repository/
    //      repository.common.cleanCheckout
    //      ?os_authType=basic&os_username=dolby&os_password=dolby
    //
    //  Set the value of a single repository configuration field    
    @POST
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.TEXT_PLAIN})
    @Path("/repository/{field}")
    public Response setRepositoryValue( @PathParam("plankey") String plankey,
                                        @PathParam("field")   String field,
                                                              String value)
    {
        log.info ("ModplanResource.setRepositoryValue(): " + value);
        try {
            Plan plan = getPlanByKey(plankey);
            return setRepositoryConfigValue(plan, field, value);
        }
        catch(ResponseException re) {
            log.error("Exception thrown in ModplanResource.setRepositoryValue()");
            return re.getResponse();
        }
    }


    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/variables")
    public Response getPlanVariables(@PathParam("plankey") String plankey)
    {
        log.info("ModplanResource.getPlanVariables()");

        try {
            Plan plan = getPlanByKey(plankey);
            return listPlanVariables(plankey);
        }
        catch(ResponseException re) {
            log.error("Exception thrown in ModplanResource.getPlanVariables()");
            return re.getResponse();
        }
    }

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/variables/{varname}")
    public Response getPlanVariable(
                    @PathParam("plankey") String plankey,
                    @PathParam("varname") String varname)
    {
        log.info("ModplanResource.getPlanVariable()");

        try {
            Plan plan = getPlanByKey(plankey);
            return getPlanVariableValue(plan, varname);
        }
        catch(ResponseException re) {
            log.error("Exception thrown in ModplanResource.getPlanVariableValue()");
            return re.getResponse();
        }
    }

    //  curl -d <value> -H "Content-Type:text/plain"
    //      http://<host>:<port>/rest/modplan/1.0/PROJ1-PLAN1/variables/foo.bar
    //      ?os_authType=basic&os_username=dolby&os_password=dolby
    //
    //  Set the value of a single plan variable
    @POST
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.TEXT_PLAIN})
    @Path("/variables/{varname}")
    public Response setPlanVariable( @PathParam("plankey") String plankey,
                                     @PathParam("varname") String varname,
                                                           String value)
    {
        log.info ("ModplanResource.setPlanVariable(): " + value);
        try {
            Plan plan = getPlanByKey(plankey);
            return setPlanVariableValue(plan, varname, value);
        }
        catch(ResponseException re) {
            log.error("Exception thrown in ModplanResource.setPlanVariableValue()");
            return re.getResponse();
        }
    }


    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/enabled")
    public Response getEnabled( @PathParam("plankey") String plankey )
    {
        log.debug("ModplanResource.getEnabled()");

        try {
            Plan plan = getPlanByKey(plankey);
            log.info("Returning plan enabled status");

            String b;
            if (plan.isSuspendedFromBuilding())
                b = "false";
            else
                b = "true";

            return Response.ok(new Message("enabled", b)).build();
        }
        catch(ResponseException re) {
            return re.getResponse();
        }
    }


    //  curl -d <value> -H "Content-Type:text/plain"
    //      http://<host>:<port>/rest/modplan/1.0/PROJ1-PLAN1/enabled
    //      ?os_authType=basic&os_username=dolby&os_password=dolby
    @POST
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.TEXT_PLAIN})
    @Path("/enabled")
    public Response setEnabled( @PathParam("plankey") String plankey,
                                                      String value)
    {
        log.info ("ModplanResource.setEnabled(): " + value);

        value = value.trim();
        try {
            Plan plan = getPlanByKey(plankey);

            String mesg = "";
            if(value.equals("suspend") || value.equals("disable")) {
                planManager.setPlanSuspendedState(plan, true);
                mesg = "Suspended plan: " + plankey;
                log.info(mesg);
                updateDashboardCache(plankey);   
            }
            else if(value.equals("resume") || value.equals("enable")) {
                planManager.setPlanSuspendedState(plan, false);
                mesg = "Resumed build: " + plankey;
                log.info(mesg);
                updateDashboardCache(plankey);   
            }
            else {
                mesg = "Invalid value: " + value;
                log.error(mesg);
                throw new ResponseException(Response.Status.BAD_REQUEST);
            }
            return Response.ok(new Message("enabled", mesg)).build();
        }
        catch(ResponseException re) {
            log.error("Exception thrown in ModplanResource.setEnabled()");
            return re.getResponse();
        }
    }


    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/types")
    public Response getTypes( @PathParam("plankey") String plankey )
    {
        log.debug("ModplanResource.getTypes()");

        Plan plan;
        try {
            plan = getPlanByKey(plankey);
        }
        catch(ResponseException re) {
            return re.getResponse();
        }

        BuildDefinition definition = plan.getBuildDefinition();
        if(definition == null) {
            log.error("BuildDefinition is null");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        String instanceList = instanceList(plan);
        return Response.ok(new Message("Instance list", instanceList)).build();

    }        

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/jobs")
    public Response getJobs( @PathParam("plankey") String plankey )
    {
        log.debug("ModplanResource.getJobs()");

        Plan plan;
        try {
            plan = getPlanByKey(plankey);
        }
        catch(ResponseException re) {
            return re.getResponse();
        }

        return listJobsForPlan(plan);
    }       

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/stages")
    public Response getStages( @PathParam("plankey") String plankey )
    {
        log.debug("ModplanResource.getStages()");

        Plan plan;
        try {
            plan = getPlanByKey(plankey);
        }
        catch(ResponseException re) {
            return re.getResponse();
        }
        return listStagesForPlan(plan);
    }       

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/jobs/{jobindex}/tasks")
    public Response getTasksForJob( @PathParam("plankey") String plankey,
                                  @PathParam("jobindex") String index)
    {
        log.debug("ModplanResource.getTasksForJob()");

        try {
            checkManagersEx();
            Job job = getJobFromPlan(plankey, index);
            return listTasksForPlan(job);
        }
        catch(ResponseException re) {
            return re.getResponse();
        }
    }       

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/jobs/{jobindex}/tasks/{taskindex}")
    public Response getTaskConfig( @PathParam("plankey")   String plankey,
                                   @PathParam("jobindex")  String jobIndex,
                                   @PathParam("taskindex") String taskIndex)
    {
        log.debug("ModplanResource.getTaskConfig()");

        try {
            checkManagersEx();
            Job job = getJobFromPlan(plankey, taskIndex);
            TaskDefinition td = getTaskDefinitionAtIndex(job, taskIndex);

            Map<String,String> map = td.getConfiguration();
            KVList k = mapToKVList(map);
            return Response.ok(k).build();
        }
        catch(ResponseException re) {
            return re.getResponse();
        }
    }

    //  http://localhost:8085/rest/modplan/1.0/PROJ1-PLAN1/jobs/0/tasks/0/argument
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/jobs/{jobindex}/tasks/{taskindex}/{field}")
    public Response getTaskConfigField( @PathParam("plankey") String plankey,
                                   @PathParam("jobindex")     String jobIndex,
                                   @PathParam("taskindex")    String taskIndex,
                                   @PathParam("field")        String field)
    {
        log.debug("ModplanResource.getTaskConfigField()");

        try {
            checkManagersEx();
            Job job = getJobFromPlan(plankey, taskIndex);
            TaskDefinition td = getTaskDefinitionAtIndex(job, taskIndex);

            Map<String,String> map = td.getConfiguration();
            if(! map.containsKey(field)) {
                log.error("No such field in task at index: " + taskIndex);
                throw new ResponseException(Response.Status.BAD_REQUEST);
            }
            return Response.ok(new Message(field, map.get(field))).build();
        }
        catch(ResponseException re) {
            return re.getResponse();
        }
    }

    //  curl -d <value> -H "Content-Type:text/plain"
    //      http://<host>:<port>/rest/modplan/1.0/PROJ1-PLAN1/jobs/0/tasks/0/argument
    @POST
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.TEXT_PLAIN})
    @Path("/jobs/{jobindex}/tasks/{taskindex}/{field}")
    public Response setTaskConfigField( @PathParam("plankey") String plankey,
                                   @PathParam("jobindex")     String jobIndex,
                                   @PathParam("taskindex")    String taskIndex,
                                   @PathParam("field")        String field,
                                                              String value)
    {
        log.debug("ModplanResource.setTaskConfigField()");

        try {
            checkManagersEx();
            Job job = getJobFromPlan(plankey, taskIndex);
            TaskDefinition td = getTaskDefinitionAtIndex(job, taskIndex);

            Map<String,String> map = td.getConfiguration();
            if(!map.containsKey(field)) {
                log.error("No such field in task at index: " + taskIndex);
                throw new ResponseException(Response.Status.BAD_REQUEST);
            }
            map.put(field, value);
            td.setConfiguration(map);
            return Response.ok(new Message(field, value)).build();
        }
        catch(ResponseException re) {
            return re.getResponse();
        }
    }


    private TaskDefinition getTaskDefinitionAtIndex(Job job, String taskIndex) throws ResponseException
    {
         int index;
         try {
             index = Integer.parseInt(taskIndex);
        }
        catch(NumberFormatException nfe) {
            log.error("Invalid integer value in path: " + taskIndex);
             throw new ResponseException(Response.Status.BAD_REQUEST);
        }

        BuildDefinition definition = job.getBuildDefinition();
        if(definition == null) {
            log.error("BuildDefinition is null");
            throw new ResponseException(Response.Status.INTERNAL_SERVER_ERROR);
        }

        java.util.List<TaskDefinition> tdlist = definition.getTaskDefinitions();


        try {
            TaskDefinition td = tdlist.get(index);
            return td;
        }
        catch(IndexOutOfBoundsException e) {
             log.error("No job at index: " + index);
             throw new ResponseException(Response.Status.BAD_REQUEST);
        }

    }

    private Job getJobFromPlan(String plankey, String index) throws ResponseException
    {
        Plan plan = planManager.getPlanByKey(plankey);
        if(plan == null) {
            log.error("No such plan:" + plankey);
            throw new ResponseException(Response.Status.BAD_REQUEST);
        }

        if(!(plan instanceof com.atlassian.bamboo.chains.Chain)) {
            log.error("Plan is not an instance of Chain");
            throw new ResponseException(Response.Status.INTERNAL_SERVER_ERROR);
        }

        Chain chain = (Chain) plan;
        java.util.List<Job> jobList = chain.getAllJobs();
        Job job;
        int indexNum;

        try {
            indexNum = Integer.parseInt(index);
        }
        catch(NumberFormatException nfe) {
            log.error("Invalid integer value in path: " + index);
             throw new ResponseException(Response.Status.BAD_REQUEST);
        }

        try {
            job = jobList.get(indexNum);
        }
        catch(IndexOutOfBoundsException ioobe) {
            log.error("No job at index: " + index);
             throw new ResponseException(Response.Status.BAD_REQUEST);
        }
        return job;
    }

    private Response listJobsForPlan(Plan plan) {

        if(!(plan instanceof com.atlassian.bamboo.chains.Chain)) {
            log.error("Plan is not an instance of Chain");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        Chain chain = (Chain) plan;
        java.util.List<Job> jobList = chain.getAllJobs();


        Iterator<Job> iter = jobList.iterator();
        StringList sl = new StringList();

        while (iter.hasNext()) {
            Job job = iter.next();
            sl.add(job.getName());
        } 

        return Response.ok(sl).build();
    }

    private Plan getPlanByKey(String planKey) throws ResponseException {

        checkManagersEx();

        Plan plan = planManager.getPlanByKey(planKey);
        if(plan == null) {
            log.error("No such plan:" + planKey);
            throw new ResponseException(Response.Status.BAD_REQUEST);
        }

        return plan;
    }


    private Response listStagesForPlan(Plan plan) {

        if(!(plan instanceof com.atlassian.bamboo.chains.Chain)) {
            log.error("Plan is not an instance of Chain");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        Chain chain = (Chain) plan;
        java.util.List<ChainStage> stageList = chain.getStages();


        Iterator<ChainStage> iter = stageList.iterator();
        StringList sl = new StringList();

        while (iter.hasNext()) {
            ChainStage stage = iter.next();
            sl.add(stage.getName());
        } 

        return Response.ok(sl).build();
    }


    private String instanceList(Plan plan) {
            
        String result = new String();

        if(plan instanceof com.atlassian.bamboo.build.Build) {
            result += "Build ";
        }
        if(plan instanceof com.atlassian.bamboo.build.Buildable) {
            result += "Buildable ";
        }
        if(plan instanceof com.atlassian.bamboo.chains.Chain) {
            result += "Chain ";
        }
        if(plan instanceof com.atlassian.bamboo.build.Job) {
            result += "Job ";
        }
        if(plan instanceof com.atlassian.bamboo.plan.TopLevelPlan) {
            result += "TopLevelPlan ";
        }

        if (plan instanceof com.atlassian.bamboo.plan.PlanPermissionSkeleton) {
            result += "PlanPermissionSkeleton ";        
        }
        
        if (result.length() == 0) {
            result = "Not an instance of any known type";
        }
        return result; 
    }


    private Response listTasksForPlan(Plan plan) {

        BuildDefinition definition = plan.getBuildDefinition();
        if(definition == null) {
            log.error("BuildDefinition is null");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        java.util.List<TaskDefinition> tdlist = definition.getTaskDefinitions();
        log.info("Number of task definitions is: " + Integer.toString(tdlist.size()));

        Iterator<TaskDefinition> iter = tdlist.iterator();
        StringList sl = new StringList();

        while (iter.hasNext()) {
            TaskDefinition td = iter.next();
            sl.add(td.getUserDescription());
        } 

        return Response.ok(sl).build();
    }


    private String mapToString(Map<String, String> map) {

        String mesg = "";
        Set<String> keys = map.keySet();
        Iterator<String> iter = keys.iterator();

        while(iter.hasNext()) {
            String key = iter.next();
            String value = map.get(key);

            String line = key + " : " + value + "\n";
            mesg += line;
        }
        return mesg;
    }

    private KVList mapToKVList(Map<String, String> map) {

        KVList list = new KVList();
        Set<String> keys = map.keySet();
        Iterator<String> iter = keys.iterator();

        while(iter.hasNext()) {
            String key = iter.next();
            String value = map.get(key);
            KVPair pair = new KVPair(key, value);
            list.addPair(pair);
        }

        return list;
    }

    private String getLastField(String s) {
        int index = s.lastIndexOf(".");
        String f = s.substring(index + 1);
        return f;
    }

    private String getPrefix(String s) {
        int index = s.lastIndexOf(".");
        String p = s.substring(0, index);
        return p;
    }


    private Response listRepositoryConfiguration(String plankey) {

        Plan plan = planManager.getPlanByKey(plankey);
        BuildDefinition definition = plan.getBuildDefinition();
        Repository repo = definition.getRepository();

        HierarchicalConfiguration config = repo.toConfiguration();
        Iterator<String> iter = config.getKeys();

        KVList list = new KVList();
        while (iter.hasNext()) {
            String key = iter.next();
            Object property = config.getProperty(key);
            KVPair pair = new KVPair(key, property.toString());
            list.addPair(pair);
        }

        return Response.ok(list).build();
    }

    private Response setRepositoryConfigValue(Plan plan, String key, String value)
    {
        BuildDefinition definition = plan.getBuildDefinition();
        Repository repo = definition.getRepository();

        HierarchicalConfiguration config = repo.toConfiguration();
        config.setProperty(key, value);
        repo.populateFromConfig(config);

        String mesg = "Set key: " + key + " to value: " + value;
        return Response.ok(new Message("default", mesg)).build();
    }


    private Response getRepositoryConfigValue(Plan plan, String key)
    {
        BuildDefinition definition = plan.getBuildDefinition();
        Repository repo = definition.getRepository();

        HierarchicalConfiguration config = repo.toConfiguration();
        String value = config.getProperty(key).toString();

        return Response.ok(new Message(key, value)).build();
    }


     private Response listPlanVariables(String plankey)
     {
         Plan plan = planManager.getPlanByKey(plankey);
         java.util.List<VariableDefinition> varList = varMgr.getPlanVariables(plan);
         Iterator<VariableDefinition> iter = varList.iterator();

         KVList list = new KVList();
         while (iter.hasNext()) {
             VariableDefinition var = iter.next();
             KVPair pair = new KVPair(var.getKey(), var.getValue());
             list.addPair(pair);
         }

         return Response.ok(list).build();
    }

    private Response getPlanVariableValue(Plan plan, String varname)  throws ResponseException
    {
        java.util.List<VariableDefinition> varList = varMgr.getPlanVariables(plan);
        Iterator<VariableDefinition> iter = varList.iterator();

        while (iter.hasNext()) {
             VariableDefinition var = iter.next();
             if (var.getKey().equals(varname)) {
                 return Response.ok(new Message(varname, var.getValue())).build();
             }
        }
         log.error("No variable '" + varname + "' defined for plan: "+ plan.getKey());
         throw new ResponseException(Response.Status.BAD_REQUEST);
    }


    private Response setPlanVariableValue(Plan plan, String varname, String value) throws ResponseException
    {
        java.util.List<VariableDefinition> varList = varMgr.getPlanVariables(plan);
        Iterator<VariableDefinition> iter = varList.iterator();

        while (iter.hasNext()) {
             VariableDefinition var = iter.next();
             if (var.getKey().equals(varname)) {
                 var.setValue(value);
                 varMgr.saveVariableDefinition(var);
                 String mesg = "Set variable: " + varname + " to value: " + value;
                return Response.ok(new Message("default", mesg)).build();
             }
        }

        // If we get here the variable doesn't yet exist
        String mesg = "Plan variable: " + varname + " does not exist";
        log.error(mesg);
        throw new ResponseException(Response.Status.INTERNAL_SERVER_ERROR);

        // VariableDefinition def = varFactory.createPlanVariable(plan, varname, value);
        //  varMgr.saveVariableDefinition(def);
        // String mesg = "Created plan variable: " + varname + " with value: " + value;
        // return Response.ok(new Message("default", mesg)).build();
    }


    private void checkManagersEx() throws ResponseException {

        if(planManager == null) {
            log.error("planManager is null");
            throw new ResponseException(Response.Status.INTERNAL_SERVER_ERROR);
        }

        if(eventManager == null) {
            log.error("eventManager is null");
            throw new ResponseException(Response.Status.INTERNAL_SERVER_ERROR);
        }

        if(dashboardCachingManager == null) {
            log.error("dashboardCachingManager is null");
            throw new ResponseException(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    public void setEventManager(EventManager eventManager)
    {
        this.eventManager = eventManager;
    }
   
    public void setPlanManager(PlanManager planManager)
    {
        this.planManager = planManager;
    }
    
    public void setDashboardCachingManager(DashboardCachingManager mgr)
    {
        this.dashboardCachingManager = mgr;
    }

    public void setVariableDefinitionManager(VariableDefinitionManager mgr)
    {
        this.varMgr = mgr;
    }

    public void setVariableDefinitionFactory(VariableDefinitionFactory factory)
    {
        this.varFactory = factory;
    }

    private void updateDashboardCache(String buildkey) {
        if(this.dashboardCachingManager != null) {    
            this.dashboardCachingManager.updatePlanCache(buildkey);
        }
    }
}


