// ---------------------------------------
// Copyright (c) 2011, Dolby Laboratories.
// See LICENSE.TXT for full license terms. 
// ---------------------------------------

package net.cysquatch.bamboo.plugins;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class KVPair {
    @XmlElement
    private String key;

    @XmlElement
    private String value;

    public KVPair() {} 

    public KVPair(String key, String value) {
        this.key   = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
