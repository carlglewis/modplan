// ---------------------------------------
// Copyright (c) 2011, Dolby Laboratories.
// See LICENSE.TXT for full license terms. 
// ---------------------------------------

package net.cysquatch.bamboo.plugins;

import javax.ws.rs.core.Response;
import java.lang.Exception;
/**
 * Created by IntelliJ IDEA.
 * User: Carl
 * Date: 8/21/11
 * Time: 11:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class ResponseException extends Exception {

    private Response mResponse;

    public ResponseException(Response r) {
        mResponse = r;
    }

    public ResponseException(Response.Status status) {
        mResponse = Response.status(status).build();
    }

    public Response getResponse() {
        return mResponse;
    }
}


